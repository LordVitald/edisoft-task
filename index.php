<?php
require __DIR__ . '/vendor/autoload.php';
use Core\Application;
use Core\Logic\Aggregations\ReportAggregation;
use Core\Repository\CsvRepository;
use Core\Logic\Values\AbstractPaymentValue;

(new Application())->Run();


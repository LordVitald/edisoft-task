<?php

namespace Core\Exceptions;

use Exception;
use Throwable;

class FileOpenException extends Exception
{
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 403, $previous);
    }
}
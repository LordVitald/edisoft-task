<?php

namespace Core\Interfaces;

interface OutPutDataSourceInterface
{
    function getOutPutData(): array;
}
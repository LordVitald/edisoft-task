<?php

namespace Core\Logic\Aggregations;

use Core\Interfaces\OutPutDataSourceInterface;
use Core\Logic\Values\AbstractPaymentValue;
use Core\Logic\Values\OrganizationValue;
use Core\Repository\AbstractDataRepository;

class ReportAggregation implements OutPutDataSourceInterface
{
    private float $totalSum = 0.0;
    private float $totalSenderSum = 0.0;
    private float $totalReceiverSum = 0.0;
    private array $payments = [];
    private array $documentTypes = [];

    private AbstractDataRepository $dataRepository;

    public function __construct(AbstractDataRepository $dataRepository)
    {
        $this->dataRepository = $dataRepository;
    }

    public function buildData()
    {
        foreach ($this->dataRepository->getData() as $dtoData) {

            $sender = new OrganizationValue(
                $dtoData->getSenderInn(),
                OrganizationValue::SENDER_PAY_TYPE,
            );

            $reciever = new OrganizationValue(
                $dtoData->getReceiverInn(),
                OrganizationValue::RECEIVER_PAY_TYPE,
            );

            $this->addNewPayment(AbstractPaymentValue::buildPaymentValue(
                $dtoData->getDocType(),
                $dtoData->getCount(),
                $sender,
                $reciever,
                $dtoData->getPayer()
            ));
        }
    }

    protected function addNewPayment(AbstractPaymentValue $payment)
    {
        $this->totalSum += $payment->getSumm();
        if ($payment->getPayer()->getType() === OrganizationValue::SENDER_PAY_TYPE) {
            $this->totalSenderSum += $payment->getSumm();
        } else {
            $this->totalReceiverSum += $payment->getSumm();
        }
        $paymentIndex = md5(
            $payment->getReceiver()->getInn() .
            $payment->getSender()->getInn() .
            $payment->getTypeDocument()
        );

        $this->payments[$paymentIndex][] = $payment;

        ++$this->documentTypes[$payment->getTypeDocument()];
    }

    private function getPercentageOfDifference()
    {
        if ($this->totalSenderSum < $this->totalReceiverSum) {
            return (($this->totalReceiverSum - $this->totalSenderSum) / $this->totalSenderSum) * 100;
        }

        return (($this->totalSenderSum - $this->totalReceiverSum) / $this->totalSenderSum) * 100;
    }

    public function getOutPutData(): array
    {
        return [
            $this->createPaymentsOutputData(),
            $this->createSummOutputData(),
            $this->createDocumentOutputData()
        ];
    }

    private function createPaymentsOutputData(): array
    {
        $payments = [
            'headers' => [
                "Плательщик",
                "Кому",
                "Тип документа",
                "Сколько"
            ]
        ];

        foreach ($this->payments as $paymentsList) {
            /**
             * @var AbstractPaymentValue $payment
             */
            foreach ($paymentsList as $payment) {
                $payments["values"][] = [
                    self::buildNameForOutput($payment->getPayer()),
                    self::buildNameForOutput($payment->getPayer()->getInn() === $payment->getReceiver()->getInn() ? $payment->getSender() : $payment->getReceiver()),
                    $payment->getTypeDocument(),
                    $payment->getSumm()
                ];
            }
        }

        return $payments;
    }

    private function createSummOutputData(): array
    {
        return [
            "headers" => [],
            "values" => [
                sprintf("Общая сумма: %.2f", $this->totalSum),
                sprintf("Сумма всех S: %.2f", $this->totalSenderSum),
                sprintf("Сумма всех R: %.2f", $this->totalReceiverSum),
                sprintf("Процентное соотношение разниц между суммами S & R: %.2f%%", $this->getPercentageOfDifference())
            ]
        ];
    }

    private function createDocumentOutputData(): array
    {
        $total = array_sum($this->documentTypes);
        $documentsData = [
            "headers" => [],
            "values" => [
                sprintf("Всего документов: %d", $total)
            ]
        ];

        foreach ($this->documentTypes as $documentType => $documentCount) {
            $documentsData["values"][] = sprintf(
                "Документов вида %s всего %d (%.2f%%)",
                $documentType,
                $documentCount,
                $documentCount * 100 / $total
            );
        }

        return $documentsData;
    }

    private static function buildNameForOutput(OrganizationValue $organization): string
    {
        return sprintf("%s (%s)", $organization->getName(), $organization->getInn());
    }
}

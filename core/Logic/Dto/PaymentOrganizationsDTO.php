<?php

namespace Core\Logic\Dto;

use Core\Logic\Values\OrganizationValue;

class PaymentOrganizationsDTO
{
    private string $senderInn;
    private string $receiverInn;
    private string $docType;
    private int $count;
    private string $payer;

    public function __construct(
        string $senderInn,
        string $receiverInn,
        string $docType,
        int    $count,
        string $payer
    )
    {
        $this->senderInn = $senderInn;
        $this->receiverInn = $receiverInn;
        $this->docType = $docType;
        $this->count = $count;
        $this->payer = $payer;
    }

    /**
     * Get the value of senderInn
     */
    public function getSenderInn(): string
    {
        return $this->senderInn;
    }

    /**
     * Get the value of recieverInn
     */
    public function getReceiverInn(): string
    {
        return $this->receiverInn;
    }

    /**
     * Get the value of docType
     */
    public function getDocType(): string
    {
        return $this->docType;
    }

    /**
     * Get the value of count
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Get the value of payer
     */
    public function getPayer(): string
    {
        return $this->payer;
    }
}

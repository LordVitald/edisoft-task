<?php

namespace Core\Logic\Values;

class ExtraLargePaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 0.4;
    }
}
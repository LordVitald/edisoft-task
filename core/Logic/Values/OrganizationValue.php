<?php

namespace Core\Logic\Values;

use Core\Logic\Dictionaries\OgranizationsDictionary;

class OrganizationValue
{

    const SENDER_PAY_TYPE = "S";
    const RECEIVER_PAY_TYPE = "R";

    private string $name;
    private string $inn;
    private string $type;

    public function __construct(string $inn, string $type)
    {
        $this->inn = $inn;
        $this->type = $type;
        $this->name = OgranizationsDictionary::getInstance()
            ->getOrganization($inn);
    }

    public function getCoefficient(): float
    {
        return self::SENDER_PAY_TYPE === $this->type ? 0.2 : 0.5;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of inn
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * Get the value of type
     */
    public function getType(): string
    {
        return $this->type;
    }
}

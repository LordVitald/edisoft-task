<?php

namespace Core\Logic\Values;

class SmallPaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 1.5;
    }
}
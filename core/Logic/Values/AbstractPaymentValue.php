<?php

namespace Core\Logic\Values;

abstract class AbstractPaymentValue
{
    private string $typeDocument;
    private int $count;
    private float $summ;
    private OrganizationValue $sender;
    private OrganizationValue $receiver;
    private string $payerType;

    private function __construct(
        string            $typeDocument,
        int               $count,
        OrganizationValue $sender,
        OrganizationValue $receiver,
        string            $payerType
    )
    {
        $this->typeDocument = $typeDocument;
        $this->count = $count;
        $this->payerType = $payerType;
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->summ = $this->calculateAmount($this->getPayer());
    }

    public static function buildPaymentValue(
        string            $typeDocument,
        int               $count,
        OrganizationValue $sender,
        OrganizationValue $receiver,
        string            $payerType
    ): AbstractPaymentValue
    {
        if ($count <= 10) {
            return new SuperSmallPaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
        }

        if ($count <= 50) {
            return new SmallPaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
        }

        if ($count <= 300) {
            return new MediumPaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
        }

        if ($count <= 1000) {
            return new MoreThanMediumPaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
        }

        if ($count <= 10000) {
            return new LargePaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
        }

        return new ExtraLargePaymentValue($typeDocument, $count, $sender, $receiver, $payerType);
    }

    protected function calculateAmount(OrganizationValue $organization): float
    {
        return static::getPrice() * $this->count * $organization->getCoefficient();
    }

    abstract static protected function getPrice();

    /**
     * Get the value of sender
     */
    public function getSender(): OrganizationValue
    {
        return $this->sender;
    }

    /**
     * Get the value of reciever
     */
    public function getReceiver(): OrganizationValue
    {
        return $this->receiver;
    }

    /**
     * Get the value of payer
     */
    public function getPayer(): OrganizationValue
    {
        return ($this->sender->getType() === $this->payerType) ? $this->sender : $this->receiver;
    }

    /**
     * Get the value of summ
     */
    public function getSumm(): float
    {
        return $this->summ;
    }

    /**
     * Get the value of typeDocuemnt
     */
    public function getTypeDocument(): string
    {
        return $this->typeDocument;
    }

    /**
     * Get the value of count
     */
    public function getCount(): int
    {
        return $this->count;
    }
}

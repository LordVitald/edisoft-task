<?php

namespace Core\Logic\Values;

class SuperSmallPaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 2.5;
    }
}
<?php

namespace Core\Logic\Values;

class MoreThanMediumPaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 0.8;
    }
}
<?php

namespace Core\Logic\Values;

class LargePaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 0.6;
    }
}
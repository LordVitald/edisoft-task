<?php

namespace Core\Logic\Values;

class MediumPaymentValue extends AbstractPaymentValue
{
    protected static function getPrice(): float
    {
        return 1.1;
    }
}
<?php

namespace Core\Logic\Dictionaries;

use Faker\Factory;

class OgranizationsDictionary
{

    private static $dictionary = null;
    private array $values = [];
    private $faker;

    private function __construct()
    {
        $this->faker = Factory::create();
    }

    public static function getInstance(): OgranizationsDictionary
    {
        if (is_null(self::$dictionary)) {
            self::$dictionary = new OgranizationsDictionary();
        }

        return self::$dictionary;
    }

    public function getOrganization(string $inn)
    {
        if (!isset($this->values[$inn])) {
            $this->values[$inn] = $this->faker->company();
        }

        return $this->values[$inn];
    }

}
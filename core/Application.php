<?php

namespace Core;

use Core\Exceptions\EmptyFileNameException;
use Core\Exceptions\FileFormatNotSupportedexception;
use Core\Logic\Aggregations\ReportAggregation;
use Core\Repository\AbstractDataRepository;
use Core\Repository\AbstractOutputRepository;
use Core\Repository\ConsoleOutputRepository;
use Core\Repository\CsvRepository;
use Core\Repository\WebOutputRepository;

class Application
{
    const FILES_FOLDER = "files";

    private ReportAggregation $report;
    private AbstractOutputRepository $output;

    /**
     * @throws EmptyFileNameException
     * @throws FileFormatNotSupportedexception
     */
    public function __construct()
    {
        if (self::isConsoleMode()) {

            $rootPath = __DIR__ . "/../" . self::FILES_FOLDER;
            $fileName = $_SERVER['argv'][1] ?? "";
            $this->output = new ConsoleOutputRepository();
        } else {
            $rootPath = $_SERVER['DOCUMENT_ROOT'] . "/" . self::FILES_FOLDER;
            $fileName = $_GET["fileName"] ?? "";
            $this->output = new WebOutputRepository();
        }

        $dataRepository = self::buildDataReporter($rootPath, $fileName);
        $this->report = new ReportAggregation($dataRepository);
    }

    private static function isConsoleMode(): bool
    {
        return $is_console = PHP_SAPI == 'cli' || (!isset($_SERVER['DOCUMENT_ROOT']) && !isset($_SERVER['REQUEST_URI']));
    }

    /**
     * @param String $rootPath
     * @param String $filename
     * @return AbstractDataRepository
     * @throws EmptyFileNameException|FileFormatNotSupportedexception
     * @throws \Exception
     */
    private static function buildDataReporter(string $rootPath, string $filename): AbstractDataRepository
    {
        if (empty($filename)) {
            throw new EmptyFileNameException("The file name cannot be empty");
        }

        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if ($ext == "csv") {
            return new CsvRepository($rootPath, $filename);
        }

        throw new FileFormatNotSupportedexception("The file format is not supported");
    }

    public function Run()
    {
        $this->report->buildData();
        $this->output->outputData($this->report);
    }
}

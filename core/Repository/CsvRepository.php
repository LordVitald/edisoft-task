<?php

namespace Core\Repository;

use Core\Exceptions\FileNotExistException;
use Core\Exceptions\FileOpenException;
use Core\Logic\Dto\PaymentOrganizationsDTO;
use Exception;

class CsvRepository extends AbstractDataRepository
{
    private function getFilePath(): string
    {
        return $this->rootPath . "/" . $this->file;
    }

    /**
     * @return boolean
     * @throws Exception|FileNotExistException
     */
    protected function validate(): bool
    {
        if (!file_exists($this->getFilePath())) {
            throw new FileNotExistException("File {$this->getFilePath()} not found");
        }

        return true;
    }

    /**
     * @return \Generator
     * @throws FileOpenException
     */
    public function getData(): \Generator
    {
        $stream = fopen($this->getFilePath(), 'r');

        if ($stream === false) {
            throw new FileOpenException("There was a problem when opening the file {$this->getFilePath()}. Check access to this file");
        }

        $needSkipFirst = true;
        while (($data = fgetcsv($stream, 1000, ",")) !== FALSE) {
            if ($needSkipFirst) {
                $needSkipFirst = false;
                continue;
            }
            yield new PaymentOrganizationsDTO(
                $data[0],
                $data[1],
                $data[2],
                $data[3],
                $data[4]
            );
        }

        fclose($stream);
    }
}

<?php

namespace Core\Repository;

use Core\Logic\Dto\PaymentOrganizationsDTO;
use Exception;
use Generator;

abstract class AbstractDataRepository
{
    protected string $file;
    protected string $rootPath;

    /**
     * @throws Exception
     */
    public function __construct(string $rootPath, string $file)
    {
        $this->file = $file;
        $this->rootPath = $rootPath;
        $this->validate();
    }

    /**
     * @return boolean
     * @throws Exception
     */
    abstract protected function validate(): bool;

    /**
     * @return PaymentOrganizationsDTO[]
     */
    abstract public function getData(): \Generator;

}

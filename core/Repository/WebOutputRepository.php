<?php

namespace Core\Repository;

use Core\Interfaces\OutPutDataSourceInterface;

class WebOutputRepository extends AbstractOutputRepository
{
    public function outputData(OutPutDataSourceInterface $source)
    {
        header('Content-Type: text/html; charset=utf-8');
        foreach ($source->getOutPutData() as $table) {
            echo "<table>";
            echo "<tr>";
            foreach ($table["headers"] as $value) {
                echo "<th>$value</th>";
            }
            echo "</tr>";
            foreach ($table["values"] as $row) {
                echo "<tr>";
                if (is_array($row)) {
                    foreach ($row as $cell) {
                        echo "<td>$cell</td>";
                    }
                    echo "</tr>";
                } else {
                    echo "<td>$row</td>";
                }
            }
            echo "</table>";
        }
    }
}

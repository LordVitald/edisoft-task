<?php

namespace Core\Repository;

use Core\Interfaces\OutPutDataSourceInterface;

abstract class AbstractOutputRepository
{
    abstract public function outputData(OutPutDataSourceInterface $source);
}
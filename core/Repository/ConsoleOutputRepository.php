<?php

namespace Core\Repository;

use Core\Interfaces\OutPutDataSourceInterface;

class ConsoleOutputRepository extends AbstractOutputRepository
{
    public function outputData(OutPutDataSourceInterface $source)
    {
        foreach ($source->getOutPutData() as $table) {
            foreach ($table["values"] as $elem) {
                if (is_array($elem)) {
                    echo implode(" ", $elem) . "\n";
                } else {
                    echo $elem . "\n";
                }
            }
        }
    }
}
